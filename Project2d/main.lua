require("AnAL")

camera_x = 0
camera_y = 0
death = 0 -- умер игрок или нет
death_alpha = 0
hero_state = "StayRight" -- направление и тип движения

-- start
start = 1 -- 0 меню, 1 видео, 2 бункер1, 3 бункер2, 4 TheEnd
menu_draw = 0
anim = 1

music = love.audio.newSource(love.sound.newSoundData("music.mp3"))
snd_splash = love.audio.newSource(love.sound.newSoundData("splash.mp3"))
video = love.graphics.newVideo("1.ogv")
video2 = love.graphics.newVideo("2.ogv")


-- Загрузка спрайтов
spr_elevator = love.graphics.newImage("elev.png")
spr_saw = love.graphics.newImage("spr_saw.png")
spr_back = love.graphics.newImage("back.png")
spr_back2 = love.graphics.newImage("back2.png")
spr_valve = love.graphics.newImage("valve.png")
spr_hero_stay_l = newAnimation(love.graphics.newImage("3.png"), 42, 42, 0.1, 0)
spr_hero_stay_r = newAnimation(love.graphics.newImage("1.png"), 42, 42, 0.1, 0)
spr_hero_go_l = newAnimation(love.graphics.newImage("4.png"), 42, 42, 0.1, 0)
spr_hero_go_r = newAnimation(love.graphics.newImage("2.png"), 42, 42, 0.1, 0)
spr_hero_splash = newAnimation(love.graphics.newImage("5.png"), 42, 42, 0.1, 0)
spr_hero_nosplash = newAnimation(love.graphics.newImage("6.png"), 42, 42, 0.1, 0)
menu1 = love.graphics.newImage("Меню1.png")
menu2 = love.graphics.newImage("Меню2.png")
menu3 = love.graphics.newImage("Меню3.png")
task = newAnimation(love.graphics.newImage("Задание.png"), 400, 45, 0.1, 0)


function free(x, y)
	for i = 1, #objects.elevator do
		if objects.elevator[i].shape:testPoint(objects.elevator[i].body:getX(), objects.elevator[i].body:getY(), 0, x, y ) then
			return false
		end
	end
	for i = 1, #objects.block do
		if objects.block[i].shape:testPoint(objects.block[i].body:getX(), objects.block[i].body:getY(), 0, x, y ) then
			return false
		end
	end
	return true
end
function free_block(x, y)
	for i = 1, #objects.block do
		if objects.block[i].shape:testPoint(objects.block[i].body:getX(), objects.block[i].body:getY(), 0, x, y ) then
			return false
		end
	end
	return true
end

function distance(x1, y1, x2, y2)
	return math.sqrt((x1-x2)*(x1-x2) + (y1-y2)*(y1-y2))
end

function sign(x)
	if x > 0 then return  1 end
	if x < 0 then return -1	end
	return 0
end

function level_load()
	--направление и тип движения
	hero_state = "StayRight"

	world = love.physics.newWorld(0, 9.81 * 64, true)
	-- координаты стен
	load_map_block = {}
	table.insert(load_map_block, {120, 120})
	table.insert(load_map_block, {120, 144})
	table.insert(load_map_block, {120, 168})
	table.insert(load_map_block, {144, 96})
	table.insert(load_map_block, {144, 192})
	table.insert(load_map_block, {144, 288})
	table.insert(load_map_block, {144, 312})
	table.insert(load_map_block, {144, 336})
	table.insert(load_map_block, {168, 96})
	table.insert(load_map_block, {168, 216})
	table.insert(load_map_block, {168, 264})
	table.insert(load_map_block, {168, 360})
	table.insert(load_map_block, {192, 96})
	table.insert(load_map_block, {192, 216})
	table.insert(load_map_block, {192, 264})
	table.insert(load_map_block, {192, 360})
	table.insert(load_map_block, {216, 96})
	table.insert(load_map_block, {216, 216})
	table.insert(load_map_block, {216, 240})
	table.insert(load_map_block, {216, 264})
	table.insert(load_map_block, {216, 360})
	table.insert(load_map_block, {240, 72})
	table.insert(load_map_block, {240, 96})
	table.insert(load_map_block, {240, 384})
	table.insert(load_map_block, {264, 48})
	table.insert(load_map_block, {264, 384})
	table.insert(load_map_block, {288, 48})
	table.insert(load_map_block, {288, 408})
	table.insert(load_map_block, {312, 48})
	table.insert(load_map_block, {312, 144})
	table.insert(load_map_block, {312, 168})
	table.insert(load_map_block, {312, 192})
	table.insert(load_map_block, {312, 216})
	table.insert(load_map_block, {312, 408})
	table.insert(load_map_block, {336, 48})
	table.insert(load_map_block, {336, 144})
	table.insert(load_map_block, {336, 192})
	table.insert(load_map_block, {336, 408})
	table.insert(load_map_block, {360, 48})
	table.insert(load_map_block, {360, 144})
	table.insert(load_map_block, {360, 168})
	table.insert(load_map_block, {360, 192})
	table.insert(load_map_block, {360, 384})
	table.insert(load_map_block, {384, 24})
	table.insert(load_map_block, {384, 168})
	table.insert(load_map_block, {384, 192})
	table.insert(load_map_block, {384, 384})
	table.insert(load_map_block, {408, 24})
	table.insert(load_map_block, {408, 168})
	table.insert(load_map_block, {408, 192})
	table.insert(load_map_block, {408, 216})
	table.insert(load_map_block, {408, 240})
	table.insert(load_map_block, {408, 360})
	table.insert(load_map_block, {408, 384})
	table.insert(load_map_block, {432, 24})
	table.insert(load_map_block, {432, 168})
	table.insert(load_map_block, {432, 240})
	table.insert(load_map_block, {432, 408})
	table.insert(load_map_block, {456, 48})
	table.insert(load_map_block, {456, 144})
	table.insert(load_map_block, {456, 240})
	table.insert(load_map_block, {456, 408})
	table.insert(load_map_block, {480, 48})
	table.insert(load_map_block, {480, 144})
	table.insert(load_map_block, {480, 240})
	table.insert(load_map_block, {480, 408})
	table.insert(load_map_block, {504, 48})
	table.insert(load_map_block, {504, 168})
	table.insert(load_map_block, {504, 240})
	table.insert(load_map_block, {504, 408})
	table.insert(load_map_block, {528, 48})
	table.insert(load_map_block, {528, 168})
	table.insert(load_map_block, {528, 216})
	table.insert(load_map_block, {528, 240})
	table.insert(load_map_block, {528, 408})
	table.insert(load_map_block, {552, 48})
	table.insert(load_map_block, {552, 168})
	table.insert(load_map_block, {552, 192})
	table.insert(load_map_block, {552, 360})
	table.insert(load_map_block, {552, 384})
	table.insert(load_map_block, {576, 48})
	table.insert(load_map_block, {576, 72})
	table.insert(load_map_block, {576, 168})
	table.insert(load_map_block, {576, 192})
	table.insert(load_map_block, {576, 360})
	table.insert(load_map_block, {600, 72})
	table.insert(load_map_block, {600, 168})
	table.insert(load_map_block, {600, 192})
	table.insert(load_map_block, {600, 360})
	table.insert(load_map_block, {624, 72})
	table.insert(load_map_block, {624, 96})
	table.insert(load_map_block, {624, 168})
	table.insert(load_map_block, {624, 192})
	table.insert(load_map_block, {624, 360})
	table.insert(load_map_block, {648, 24})
	table.insert(load_map_block, {648, 48})
	table.insert(load_map_block, {648, 72})
	table.insert(load_map_block, {648, 96})
	table.insert(load_map_block, {648, 168})
	table.insert(load_map_block, {648, 192})
	table.insert(load_map_block, {648, 360})
	table.insert(load_map_block, {672, 24})
	table.insert(load_map_block, {672, 192})
	table.insert(load_map_block, {672, 216})
	table.insert(load_map_block, {672, 360})
	table.insert(load_map_block, {696, 24})
	table.insert(load_map_block, {696, 192})
	table.insert(load_map_block, {696, 240})
	table.insert(load_map_block, {696, 264})
	table.insert(load_map_block, {696, 288})
	table.insert(load_map_block, {696, 312})
	table.insert(load_map_block, {696, 336})
	table.insert(load_map_block, {720, 24})
	table.insert(load_map_block, {720, 192})
	table.insert(load_map_block, {720, 240})
	table.insert(load_map_block, {720, 264})
	table.insert(load_map_block, {720, 288})
	table.insert(load_map_block, {720, 312})
	table.insert(load_map_block, {720, 336})
	table.insert(load_map_block, {744, 24})
	table.insert(load_map_block, {744, 48})
	table.insert(load_map_block, {744, 120})
	table.insert(load_map_block, {744, 144})
	table.insert(load_map_block, {744, 168})
	table.insert(load_map_block, {744, 192})
	table.insert(load_map_block, {744, 216})
	table.insert(load_map_block, {744, 360})
	table.insert(load_map_block, {768, 48})
	table.insert(load_map_block, {768, 120})
	table.insert(load_map_block, {768, 144})
	table.insert(load_map_block, {768, 168})
	table.insert(load_map_block, {768, 192})
	table.insert(load_map_block, {768, 216})
	table.insert(load_map_block, {768, 384})
	table.insert(load_map_block, {792, 48})
	table.insert(load_map_block, {792, 384})
	table.insert(load_map_block, {816, 48})
	table.insert(load_map_block, {816, 384})
	table.insert(load_map_block, {840, 48})
	table.insert(load_map_block, {840, 72})
	table.insert(load_map_block, {840, 96})
	table.insert(load_map_block, {840, 120})
	table.insert(load_map_block, {840, 144})
	table.insert(load_map_block, {840, 168})
	table.insert(load_map_block, {840, 192})
	table.insert(load_map_block, {840, 216})
	table.insert(load_map_block, {840, 384})
	table.insert(load_map_block, {864, 72})
	table.insert(load_map_block, {864, 96})
	table.insert(load_map_block, {864, 120})
	table.insert(load_map_block, {864, 144})
	table.insert(load_map_block, {864, 168})
	table.insert(load_map_block, {864, 192})
	table.insert(load_map_block, {864, 216})
	table.insert(load_map_block, {864, 384})
	table.insert(load_map_block, {888, 72})
	table.insert(load_map_block, {888, 96})
	table.insert(load_map_block, {888, 408})
	table.insert(load_map_block, {912, 72})
	table.insert(load_map_block, {912, 408})
	table.insert(load_map_block, {936, 72})
	table.insert(load_map_block, {936, 408})
	table.insert(load_map_block, {960, 240})
	table.insert(load_map_block, {960, 264})
	table.insert(load_map_block, {960, 288})
	table.insert(load_map_block, {960, 312})
	table.insert(load_map_block, {960, 336})
	table.insert(load_map_block, {960, 360})
	table.insert(load_map_block, {960, 384})
	table.insert(load_map_block, {984, 240})
	table.insert(load_map_block, {1008, 240})
	table.insert(load_map_block, {1032, 240})
	table.insert(load_map_block, {1056, 240})
	table.insert(load_map_block, {1080, 240})

	-- координаты лифтов
	load_map_elevator = {}
	table.insert(load_map_elevator, {240, 216})
	table.insert(load_map_elevator, {288, 240})
	table.insert(load_map_elevator, {384, 48})
	table.insert(load_map_elevator, {672, 168})
	table.insert(load_map_elevator, {888, 384})
	table.insert(load_map_elevator, {336, 216})
	table.insert(load_map_elevator, {432, 360})

	-- координаты пил
	load_map_saw = {}
	table.insert(load_map_saw, {552 , 216})
	table.insert(load_map_saw, {624 , 216})
	table.insert(load_map_saw, {528, 72})

	-- координаты вентилей
	load_map_valve = {}
	table.insert(load_map_valve, {216, 192})
	table.insert(load_map_valve, {168, 192})
	table.insert(load_map_valve, {312, 120})
	table.insert(load_map_valve, {192, 336})
	table.insert(load_map_valve, {360, 120})
	table.insert(load_map_valve, {240, 336})
	table.insert(load_map_valve, {552, 336})
	table.insert(load_map_valve, {648, 336})
	table.insert(load_map_valve, {456-24, 336})
	table.insert(load_map_valve, {504+24, 336})
	table.insert(load_map_valve, {648, 168-24})
	table.insert(load_map_valve, {864, 360})
	table.insert(load_map_valve, {720, 168})
	table.insert(load_map_valve, {816, 360})

	-- координаты пил
	load_map_spike = {}
	table.insert(load_map_spike, {168, 288})
	table.insert(load_map_spike, {168, 312})
	table.insert(load_map_spike, {168, 336})
	table.insert(load_map_spike, {216, 120})
	table.insert(load_map_spike, {240, 120})
	table.insert(load_map_spike, {240, 240})
	table.insert(load_map_spike, {240, 360})
	table.insert(load_map_spike, {264, 360})
	table.insert(load_map_spike, {288, 384})
	table.insert(load_map_spike, {312, 240})
	table.insert(load_map_spike, {312, 384})
	table.insert(load_map_spike, {336, 216})
	table.insert(load_map_spike, {336, 384})
	table.insert(load_map_spike, {360, 216})
	table.insert(load_map_spike, {360, 360})
	table.insert(load_map_spike, {384, 48})
	table.insert(load_map_spike, {384, 144})
	table.insert(load_map_spike, {384, 216})
	table.insert(load_map_spike, {384, 360})
	table.insert(load_map_spike, {408, 48})
	table.insert(load_map_spike, {408, 144})
	table.insert(load_map_spike, {432, 48})
	table.insert(load_map_spike, {432, 144})
	table.insert(load_map_spike, {432, 384})
	table.insert(load_map_spike, {456, 384})
	table.insert(load_map_spike, {480, 384})
	table.insert(load_map_spike, {504, 384})
	table.insert(load_map_spike, {528, 384})
	table.insert(load_map_spike, {672, 48})
	table.insert(load_map_spike, {720, 48})
	table.insert(load_map_spike, {744, 336})
	table.insert(load_map_spike, {768, 360})
	table.insert(load_map_spike, {792, 360})
	table.insert(load_map_spike, {888, 120})
	table.insert(load_map_spike, {888, 384})
	table.insert(load_map_spike, {912, 96})
	table.insert(load_map_spike, {912, 384})
	table.insert(load_map_spike, {936, 96})
	table.insert(load_map_spike, {936, 384})

	-- список игровых объектов
	objects = {}

	-- добавляем игрока
	objects.player = {}
	objects.player.body = love.physics.newBody(world, 168 + 24 , 168 + 24, "dynamic")
	objects.player.shape = love.physics.newCircleShape(18)
	objects.player.fixture = love.physics.newFixture(objects.player.body, objects.player.shape, 0)
	objects.player.fixture:setFriction(1.8)
	objects.player.fixture:setRestitution(0.1)

	-- добавляем пилы
	objects.saw = {}
	for i = 1, #load_map_saw do
		objects.saw[i] = {}
		objects.saw[i].x = load_map_saw[i][1] + 24
		objects.saw[i].y = load_map_saw[i][2] + 24
		objects.saw[i].hspeed = 0
		objects.saw[i].vspeed = 0
		objects.saw[i].image_angle = 0
	end

	-- брызги крови
	objects.blood = {}

	-- добавляем лифты
	objects.elevator = {}
	for i = 1, #load_map_elevator do
		objects.elevator[i] = {}
		objects.elevator[i].body = love.physics.newBody(world, load_map_elevator[i][1] + 36, load_map_elevator[i][2] + 12, "static")
		objects.elevator[i].shape = love.physics.newRectangleShape(0, 0, 24*3, 23)
		objects.elevator[i].fixture = love.physics.newFixture(objects.elevator[i].body, objects.elevator[i].shape, 0)
		objects.elevator[i].vspeed = 0
		objects.elevator[i].hspeed = 0
	end

	-- добавляем стены
	objects.block = {}
	for i = 1, #load_map_block do
		objects.block[i] = {}
		objects.block[i].body = love.physics.newBody(world, load_map_block[i][1] + 12, load_map_block[i][2] + 12, "static")
		objects.block[i].shape = love.physics.newRectangleShape(0, 0, 24, 24)
		objects.block[i].fixture = love.physics.newFixture(objects.block[i].body, objects.block[i].shape, 0)
	end

	-- добавляем шипы
	objects.spike = {}
	for i = 1, #load_map_spike do
		objects.spike[i] = {}
		objects.spike[i].x = load_map_spike[i][1] + 12
		objects.spike[i].y = load_map_spike[i][2] + 12
	end

	-- добавляем вентили
	objects.valve = {}
	for i = 1, #load_map_valve do
		objects.valve[i] = {}
		objects.valve[i].x = load_map_valve[i][1] + 12
		objects.valve[i].y = load_map_valve[i][2] + 12
		objects.valve[i].angle = 0
	end
	-- обнуляем состояние смерти
	--death_alpha = 0
end

function level_load2()
	--направление и тип движения
	hero_state = "StayRight"

	world = love.physics.newWorld(0, 9.81 * 64, true)
	-- координаты стен
	load_map_block = {}
	table.insert(load_map_block, {0, 168})
	table.insert(load_map_block, {24, 192})
	table.insert(load_map_block, {48, 192})
	table.insert(load_map_block, {72, 120})
	table.insert(load_map_block, {72, 192})
	table.insert(load_map_block, {96, 120})
	table.insert(load_map_block, {96, 192})
	table.insert(load_map_block, {120, 120})
	table.insert(load_map_block, {120, 192})
	table.insert(load_map_block, {120, 216})
	table.insert(load_map_block, {120, 240})
	table.insert(load_map_block, {120, 264})
	table.insert(load_map_block, {120, 288})
	table.insert(load_map_block, {120, 312})
	table.insert(load_map_block, {120, 336})
	table.insert(load_map_block, {120, 360})
	table.insert(load_map_block, {144, 120})
	table.insert(load_map_block, {144, 384})
	table.insert(load_map_block, {168, 120})
	table.insert(load_map_block, {168, 408})
	table.insert(load_map_block, {192, 120})
	table.insert(load_map_block, {192, 408})
	table.insert(load_map_block, {216, 120})
	table.insert(load_map_block, {216, 408})
	table.insert(load_map_block, {240, 120})
	table.insert(load_map_block, {240, 408})
	table.insert(load_map_block, {264, 120})
	table.insert(load_map_block, {264, 408})
	table.insert(load_map_block, {288, 120})
	table.insert(load_map_block, {288, 408})
	table.insert(load_map_block, {312, 120})
	table.insert(load_map_block, {312, 408})
	table.insert(load_map_block, {336, 120})
	table.insert(load_map_block, {336, 408})
	table.insert(load_map_block, {360, 120})
	table.insert(load_map_block, {360, 408})
	table.insert(load_map_block, {384, 120})
	table.insert(load_map_block, {384, 408})
	table.insert(load_map_block, {408, 120})
	table.insert(load_map_block, {408, 408})
	table.insert(load_map_block, {432, 120})
	table.insert(load_map_block, {432, 408})
	table.insert(load_map_block, {456, 120})
	table.insert(load_map_block, {456, 408})
	table.insert(load_map_block, {480, 120})
	table.insert(load_map_block, {480, 408})
	table.insert(load_map_block, {504, 120})
	table.insert(load_map_block, {504, 408})
	table.insert(load_map_block, {528, 120})
	table.insert(load_map_block, {528, 408})
	table.insert(load_map_block, {552, 120})
	table.insert(load_map_block, {552, 408})
	table.insert(load_map_block, {576, 120})
	table.insert(load_map_block, {576, 408})
	table.insert(load_map_block, {600, 120})
	table.insert(load_map_block, {600, 384})
	table.insert(load_map_block, {624, 120})
	table.insert(load_map_block, {624, 192})
	table.insert(load_map_block, {624, 216})
	table.insert(load_map_block, {624, 240})
	table.insert(load_map_block, {624, 264})
	table.insert(load_map_block, {624, 288})
	table.insert(load_map_block, {624, 312})
	table.insert(load_map_block, {624, 336})
	table.insert(load_map_block, {624, 360})
	table.insert(load_map_block, {648, 96})
	table.insert(load_map_block, {648, 120})
	table.insert(load_map_block, {648, 192})
	table.insert(load_map_block, {648, 216})
	table.insert(load_map_block, {672, 72})
	table.insert(load_map_block, {672, 192})
	table.insert(load_map_block, {672, 240})
	table.insert(load_map_block, {696, 72})
	table.insert(load_map_block, {696, 192})
	table.insert(load_map_block, {696, 240})
	table.insert(load_map_block, {720, 72})
	table.insert(load_map_block, {720, 192})
	table.insert(load_map_block, {720, 240})
	table.insert(load_map_block, {720, 264})
	table.insert(load_map_block, {720, 288})
	table.insert(load_map_block, {720, 312})
	table.insert(load_map_block, {720, 336})
	table.insert(load_map_block, {720, 360})
	table.insert(load_map_block, {744, 72})
	table.insert(load_map_block, {744, 384})
	table.insert(load_map_block, {768, 72})
	table.insert(load_map_block, {768, 96})
	table.insert(load_map_block, {768, 120})
	table.insert(load_map_block, {768, 384})
	table.insert(load_map_block, {792, 96})
	table.insert(load_map_block, {792, 120})
	table.insert(load_map_block, {792, 384})
	table.insert(load_map_block, {816, 192})
	table.insert(load_map_block, {816, 216})
	table.insert(load_map_block, {816, 360})
	table.insert(load_map_block, {840, 192})
	table.insert(load_map_block, {840, 216})
	table.insert(load_map_block, {840, 360})
	table.insert(load_map_block, {864, 72})
	table.insert(load_map_block, {864, 192})
	table.insert(load_map_block, {864, 216})
	table.insert(load_map_block, {864, 360})
	table.insert(load_map_block, {888, 72})
	table.insert(load_map_block, {888, 192})
	table.insert(load_map_block, {888, 216})
	table.insert(load_map_block, {888, 360})
	table.insert(load_map_block, {912, 72})
	table.insert(load_map_block, {912, 192})
	table.insert(load_map_block, {912, 216})
	table.insert(load_map_block, {912, 360})
	table.insert(load_map_block, {936, 192})
	table.insert(load_map_block, {936, 216})
	table.insert(load_map_block, {936, 360})
	table.insert(load_map_block, {960, 192})
	table.insert(load_map_block, {960, 216})
	table.insert(load_map_block, {960, 360})
	table.insert(load_map_block, {984, 48})
	table.insert(load_map_block, {984, 72})
	table.insert(load_map_block, {984, 96})
	table.insert(load_map_block, {984, 120})
	table.insert(load_map_block, {984, 144})
	table.insert(load_map_block, {984, 168})
	table.insert(load_map_block, {984, 192})
	table.insert(load_map_block, {984, 216})
	table.insert(load_map_block, {984, 240})
	table.insert(load_map_block, {984, 264})
	table.insert(load_map_block, {984, 288})
	table.insert(load_map_block, {984, 312})
	table.insert(load_map_block, {984, 336})
	table.insert(load_map_block, {384, 240})


	-- координаты лифтов
	load_map_elevator = {}
	table.insert(load_map_elevator, {144, 240})
	table.insert(load_map_elevator, {336, 312})
	table.insert(load_map_elevator, {552, 312})
	table.insert(load_map_elevator, {744, 192})
	table.insert(load_map_elevator, {744, 216})

	-- координаты пил
	load_map_saw = {}
	table.insert(load_map_saw, {168, 144})
	table.insert(load_map_saw, {288, 144})
	table.insert(load_map_saw, {408, 144})
	table.insert(load_map_saw, {504, 144})
	table.insert(load_map_saw, {240, 312+24+24})
	table.insert(load_map_saw, {288, 336+24})
	table.insert(load_map_saw, {336, 360})
	table.insert(load_map_saw, {456, 360})
	table.insert(load_map_saw, {552, 360})
	table.insert(load_map_saw, {672, 96+48})
	table.insert(load_map_saw, {816, 96})
	table.insert(load_map_saw, {864, 96})
	table.insert(load_map_saw, {936, 144})
	table.insert(load_map_saw, {816, 240})
	table.insert(load_map_saw, {864, 240})
	table.insert(load_map_saw, {912, 240})

	-- координаты вентилей
	load_map_valve = {}
	table.insert(load_map_valve, {48, 168})
	table.insert(load_map_valve, {96, 168})
	table.insert(load_map_valve, {144, 216})
	table.insert(load_map_valve, {216, 216})
	table.insert(load_map_valve, {288, 216})
	table.insert(load_map_valve, {336, 288})
	table.insert(load_map_valve, {360, 216})
	table.insert(load_map_valve, {432, 288})
	table.insert(load_map_valve, {552, 288})
	table.insert(load_map_valve, {600, 288})
	table.insert(load_map_valve, {720, 168})
	table.insert(load_map_valve, {816, 168})
	table.insert(load_map_valve, {864, 168})
	table.insert(load_map_valve, {912, 168})
	table.insert(load_map_valve, {816, 336})
	table.insert(load_map_valve, {864, 336})
	table.insert(load_map_valve, {912, 336})
	table.insert(load_map_valve, {960, 336})

	-- координаты пил
	load_map_spike = {}
	table.insert(load_map_spike, {144, 360})
	table.insert(load_map_spike, {168, 384})
	table.insert(load_map_spike, {192, 384})
	table.insert(load_map_spike, {216, 384})
	table.insert(load_map_spike, {240, 384})
	table.insert(load_map_spike, {264, 384})
	table.insert(load_map_spike, {288, 384})
	table.insert(load_map_spike, {312, 384})
	table.insert(load_map_spike, {336, 384})
	table.insert(load_map_spike, {360, 384})
	table.insert(load_map_spike, {384, 384})
	table.insert(load_map_spike, {408, 240})
	table.insert(load_map_spike, {408, 384})
	table.insert(load_map_spike, {432, 384})
	table.insert(load_map_spike, {456, 384})
	table.insert(load_map_spike, {480, 384})
	table.insert(load_map_spike, {504, 384})
	table.insert(load_map_spike, {528, 144})
	table.insert(load_map_spike, {528, 384})
	table.insert(load_map_spike, {552, 144})
	table.insert(load_map_spike, {552, 384})
	table.insert(load_map_spike, {576, 384})
	table.insert(load_map_spike, {600, 240})
	table.insert(load_map_spike, {600, 360})
	table.insert(load_map_spike, {744, 288})
	table.insert(load_map_spike, {744, 312})
	table.insert(load_map_spike, {744, 360})
	table.insert(load_map_spike, {768, 360})
	table.insert(load_map_spike, {792, 192})
	table.insert(load_map_spike, {792, 216})
	table.insert(load_map_spike, {792, 360})

	-- список игровых объектов
	objects = {}

	-- добавляем игрока
	objects.player = {}
	objects.player.body = love.physics.newBody(world, 24 + 24 , 144 + 24, "dynamic")
	objects.player.shape = love.physics.newCircleShape(18)
	objects.player.fixture = love.physics.newFixture(objects.player.body, objects.player.shape, 0)
	objects.player.fixture:setFriction(1.8)
	objects.player.fixture:setRestitution(0.1)

	-- добавляем пилы
	objects.saw = {}
	for i = 1, #load_map_saw do
		objects.saw[i] = {}
		objects.saw[i].x = load_map_saw[i][1] + 24
		objects.saw[i].y = load_map_saw[i][2] + 24
		objects.saw[i].hspeed = 0
		objects.saw[i].vspeed = 0
		objects.saw[i].image_angle = 0
	end

	-- брызги крови
	objects.blood = {}

	-- добавляем лифты
	objects.elevator = {}
	for i = 1, #load_map_elevator do
		objects.elevator[i] = {}
		objects.elevator[i].body = love.physics.newBody(world, load_map_elevator[i][1] + 36, load_map_elevator[i][2] + 12, "static")
		objects.elevator[i].shape = love.physics.newRectangleShape(0, 0, 24*3, 23)
		objects.elevator[i].fixture = love.physics.newFixture(objects.elevator[i].body, objects.elevator[i].shape, 0)
		objects.elevator[i].vspeed = 0
		objects.elevator[i].hspeed = 0
	end

	-- добавляем стены
	objects.block = {}
	for i = 1, #load_map_block do
		objects.block[i] = {}
		objects.block[i].body = love.physics.newBody(world, load_map_block[i][1] + 12, load_map_block[i][2] + 12, "static")
		objects.block[i].shape = love.physics.newRectangleShape(0, 0, 24, 24)
		objects.block[i].fixture = love.physics.newFixture(objects.block[i].body, objects.block[i].shape, 0)
	end

	-- добавляем шипы
	objects.spike = {}
	for i = 1, #load_map_spike do
		objects.spike[i] = {}
		objects.spike[i].x = load_map_spike[i][1] + 12
		objects.spike[i].y = load_map_spike[i][2] + 12
	end

	-- добавляем вентили
	objects.valve = {}
	for i = 1, #load_map_valve do
		objects.valve[i] = {}
		objects.valve[i].x = load_map_valve[i][1] + 12
		objects.valve[i].y = load_map_valve[i][2] + 12
		objects.valve[i].angle = 0
	end
	-- обнуляем состояние смерти
	--death_alpha = 0
end

function love.load()
	love.window.setMode(640, 480) -- размеры окна
	love.window.setTitle("Oila") -- заголовок окна

	love.physics.setMeter(64)

	-- Ширина и высота уровня
	map_width = 1080
	map_height = 480
	-- Начальные установки окна


end

-- если кнопка отпущена
function love.keyreleased(key)
	if key == "left" then
		if hero_state == "GoLeft" then
			hero_state = "StayLeft"
			objects.player.body:setLinearVelocity( 0, vely);
		end
	end
	if key == "right" then
		if hero_state == "GoRight" then
			hero_state = "StayRight"
			objects.player.body:setLinearVelocity( 0, vely);
		end
	end
end

function love.update(dt)

	if (start == 0 and love.keyboard.isDown("space")) then
		video:pause()
		music:play()
		music:setLooping(true)
		start = 2
	end

	if start == 4 then
		love.audio.stop(music)
	end
	
	if (start == 4 and love.keyboard.isDown("space")) then
		love.event.quit()
	end

	if (start == 1) then
	mouse_x, mouse_y = love.mouse.getPosition()
	menu_draw = 0
		if ((mouse_x > 16 and mouse_x < 281) and (mouse_y > 401 and mouse_y < 446)) then
			menu_draw = 1
			if (love.mouse.isDown(1)) then level_load(); start = 0; video:play(); end
		end

		if ((mouse_x > 321 and mouse_x < 585) and (mouse_y > 401 and mouse_y < 446)) then
			menu_draw = 2
			if (love.mouse.isDown(1)) then love.event.quit() end
		end
	end

	if (start == 2 or start == 3) then

		--задание
		if(task:getCurrentFrame() < task:getSize()) then
			task:update(dt)	--задание
		end

		world:update(dt)

		px = objects.player.body:getX()
		py = objects.player.body:getY()

		-- отследить смерть игрока
		if death == 0 then
			x = objects.player.body:getX()
			y = objects.player.body:getY()
			if (not (free(x + 10, y + 0) and free(x - 10, y + 0) and free(x + 0, y + 10) and free(x + 0, y - 10) )) then
				death = 1
			end

			for i = 1, #objects.saw do
				sx = objects.saw[i].x
				sy = objects.saw[i].y
				if distance(px, py, sx, sy) < 24 + 16 then
					death = 1
				end
			end
			for i = 1, #objects.spike do
				sx = objects.spike[i].x
				sy = objects.spike[i].y
				if distance(px, py, sx+12, sy+12) < 14 or
					 distance(px, py, sx+12, sy-12) < 14 or
					 distance(px, py, sx-12, sy-12) < 14 or
					 distance(px, py, sx-12, sy+12) < 14 then
					death = 1
				end
			end
			if death == 1 then
				-- добавляем кровь
				math.randomseed(objects.player.body:getX() + objects.player.body:getY() + math.random(10))
				objects.blood = {}
				for i = 1, 25 do
					objects.blood[i] = {}
					objects.blood[i].x = {}
					objects.blood[i].y = {}
					for j = 1, 50 do
						objects.blood[i].x[j] = objects.player.body:getX()
						objects.blood[i].y[j] = objects.player.body:getY()
					end
					objects.blood[i].ws = (math.random()*100-50)/2
					objects.blood[i].hs = (math.random()*100-50)/2
					objects.blood[i].k = math.random(5)
				end
			end
		end

		if (px >= 936 and py<240 and start == 3) then
			start = 4
			video2:play()
		end

		if (px >= 1080 and start == 2) then
			level_load2()
			start = 3
		end

		-- ездим на платформах
		hit = 0
		for i = 1, #objects.elevator do
			if (objects.elevator[i].shape:testPoint(objects.elevator[i].body:getX(), objects.elevator[i].body:getY(), 0, px, py+26)) then
				if free(px + 25*objects.elevator[i].hspeed, py + 25*objects.elevator[i].vspeed) then
					objects.player.body:setX(px + objects.elevator[i].hspeed)
					objects.player.body:setY(py + objects.elevator[i].vspeed)
				end
				if (objects.elevator[i].vspeed > 0) then
				objects.player.body:setY(py + objects.elevator[i].vspeed)
				end
			end
		end

		-- пермещение камеры
		if death == 0 then
			ww , wh, _ =  love.window.getMode()
			camera_x = math.floor(objects.player.body:getX() - ww/2)
			camera_y = math.floor(objects.player.body:getY() - wh/2)
			camera_x =  math.max(camera_x, 0)
			camera_x =  math.min(camera_x, map_width - 640)
			camera_y =  math.max(camera_y, 0)
			camera_y =  math.min(camera_y, map_height - 480)
		end

		if love.keyboard.isDown("right") and free(objects.player.body:getX()+25, objects.player.body:getY()) then
			if (anim == 1) then
				spr_hero_go_r:update(dt)
				hero_state = "GoRight"
			end
			velx, vely = objects.player.body:getLinearVelocity()
			objects.player.body:setLinearVelocity( 100, vely);
		end

		if love.keyboard.isDown("left") and free(objects.player.body:getX()-25, objects.player.body:getY())then
			if (anim == 1) then
				spr_hero_go_l:update(dt)
				hero_state = "GoLeft"
			end
			velx, vely = objects.player.body:getLinearVelocity()
			objects.player.body:setLinearVelocity( -100, vely)
		end

		if not love.keyboard.isDown("right") and not love.keyboard.isDown("left") then
			velx, vely = objects.player.body:getLinearVelocity()
			objects.player.body:setLinearVelocity( velx/1.5, vely);
		end

		if love.keyboard.isDown("up") then

			snd_splash:play()
			-- вентили
			for i = 1, #objects.valve do
				if objects.player.shape:testPoint(objects.player.body:getX(), objects.player.body:getY(), 0, objects.valve[i].x, objects.valve[i].y) then
						objects.valve[i].angle = -3.14
						-- switch case :(
					if start == 2 then
						if (i == 1) then objects.elevator[1].vspeed = -1 end
						if (i == 2) then objects.elevator[1].vspeed = 1 end
						if (i == 3) then objects.elevator[1].vspeed = 1; objects.saw[3].hspeed = -2 end
						if (i == 4) then objects.elevator[6].vspeed = 1 end
						if (i == 5) then objects.elevator[2].vspeed = 1 end
						if (i == 6) then objects.elevator[1].vspeed = -1 end
						if (i == 9) then objects.elevator[7].hspeed = 1 end
						if (i == 10) then objects.elevator[7].hspeed = -1 end
						if (i == 8) then objects.elevator[3].vspeed = 1 end
						if (i == 7) then objects.saw[1].vspeed = 2; objects.saw[2].vspeed = 3 end
						if (i == 11) then objects.elevator[4].vspeed = -1 end
						if (i == 12) then objects.elevator[5].vspeed = -1 end
						if (i == 13) then objects.elevator[4].vspeed = 1 end
						if (i == 14) then objects.elevator[5].vspeed = 1 end
					else
						if (i == 1) then objects.saw[10].hspeed = 1; end
						if (i == 2) then objects.saw[1].vspeed = 1; end
						if (i == 5) then objects.saw[7].vspeed = -1; end
						if (i == 3) then objects.saw[1].vspeed = -1; objects.saw[5].vspeed = -1; objects.saw[6].vspeed = -1; end
						if (i == 8) then objects.saw[4].vspeed = 1; objects.saw[9].vspeed = -1; end
						if (i == 4) then objects.elevator[1].hspeed = 1; objects.saw[5].vspeed = -1; objects.saw[6].vspeed = -1;  objects.saw[2].vspeed = 1 end
						if (i == 7) then objects.elevator[1].hspeed = -1; objects.saw[7].vspeed = -1; objects.saw[3].vspeed = 1; end
						if (i == 6) then objects.elevator[2].hspeed = 1; end
						if (i == 9) then objects.elevator[3].hspeed = -1; objects.elevator[3].vspeed = -1; objects.elevator[2].hspeed = -1; end
						if (i == 10) then objects.elevator[3].vspeed = -1; objects.saw[9].vspeed = -1; end
						if (i == 12) then objects.elevator[5].hspeed = -1; objects.saw[10].hspeed = 1; end
						if (i == 11) then objects.elevator[4].vspeed = 1; end
						if (i == 15) then objects.elevator[4].vspeed = -1; objects.saw[11].vspeed = -1; end
						if (i == 13) then objects.saw[14].vspeed = 1; objects.saw[11].vspeed = 1; end
						if (i == 14) then objects.saw[11].vspeed = 1; objects.saw[12].vspeed = 1; end
						if (i == 17) then objects.saw[13].vspeed = -1; end
						if (i == 16) then objects.saw[14].vspeed = 1; objects.saw[15].vspeed = 1; objects.saw[16].vspeed = 1; end
						if (i == 18) then objects.elevator[4].vspeed = -1; end
					end
				end
			end
			hero_state = "Splash"
		end

		if (hero_state == "Splash") then
			size = spr_hero_splash:getSize()
			frame = spr_hero_splash:getCurrentFrame()
			if(frame < size) then
				anim = 0
				spr_hero_splash:update(dt)
			else
				anim = 1
				frame = spr_hero_splash:setCurrentFrame(1)
				hero_state = "NoSplash"
			end
		end

		-- движение платформ
		for i = 1, #objects.elevator do
			 if (free(objects.elevator[i].body:getX(), objects.elevator[i].body:getY() + 13*sign(objects.elevator[i].vspeed)) and
					 free(objects.elevator[i].body:getX()+24, objects.elevator[i].body:getY() + 13*sign(objects.elevator[i].vspeed)) and
					 free(objects.elevator[i].body:getX()-24, objects.elevator[i].body:getY() + 13*sign(objects.elevator[i].vspeed))) then
					objects.elevator[i].body:setY(objects.elevator[i].body:getY()+sign(objects.elevator[i].vspeed))
				else
					objects.elevator[i].vspeed = 0
				end
				if (free(objects.elevator[i].body:getX() + 37*sign(objects.elevator[i].hspeed), objects.elevator[i].body:getY())) then
					objects.elevator[i].body:setX(objects.elevator[i].body:getX()+sign(objects.elevator[i].hspeed))
					 else
					objects.elevator[i].hspeed = 0
				end
		end

		-- движение пил
		for i = 1, #objects.saw do

			if (math.abs(objects.saw[i].vspeed) > 0) then
				if (free_block(objects.saw[i].x, objects.saw[i].y + 25*sign(objects.saw[i].vspeed))) then
					objects.saw[i].y = objects.saw[i].y + sign(objects.saw[i].vspeed)
				else
					objects.saw[i].vspeed = -objects.saw[i].vspeed
				end
			end
			if (math.abs(objects.saw[i].hspeed) > 0) then
				if (free_block(objects.saw[i].x + 25*sign(objects.saw[i].hspeed), objects.saw[i].y)) then
					objects.saw[i].x = objects.saw[i].x + sign(objects.saw[i].hspeed)
				else
					objects.saw[i].hspeed = -objects.saw[i].hspeed
				end
			end
		end
	end
end

function love.draw()

	if (start == 0) then -- видео
		love.graphics.draw(video, 0, 0)
	end

	if (start == 1) then -- меню
		if (menu_draw == 0) then love.graphics.draw(menu1) end
		if (menu_draw == 1) then love.graphics.draw(menu2) end
		if (menu_draw == 2) then love.graphics.draw(menu3) end
	end

	if (start == 4) then -- меню
		love.graphics.draw(video2, 0, 0)
	end

	if (start == 2 or start == 3) then -- бункер 1

		-- рисуем фон
		if start == 2 then
			love.graphics.draw(spr_back, -camera_x, -camera_y)
		else
			love.graphics.draw(spr_back2, -camera_x, -camera_y)
		end

		-- рисуем вентили
		for i = 1, #objects.valve do
			love.graphics.draw(spr_valve, objects.valve[i].x - camera_x, objects.valve[i].y - camera_y, objects.valve[i].angle, 1, 1, 12, 12, 0, 0)
			if objects.valve[i].angle < 0 then objects.valve[i].angle = objects.valve[i].angle + 0.04 end
		end

		-- рисуем платформы
		for i = 1, #objects.elevator do
			love.graphics.draw(spr_elevator, objects.elevator[i].body:getX() - camera_x, objects.elevator[i].body:getY() - camera_y, 0, 1, 1, 36, 12, 0, 0)
		end

		-- рисуем циркулярки
		for i = 1, #objects.saw do
			love.graphics.draw(spr_saw, objects.saw[i].x - camera_x, objects.saw[i].y - camera_y, objects.saw[i].image_angle, 1, 1, 24, 24, 0, 0)
			objects.saw[i].image_angle = objects.saw[i].image_angle + 0.05
		end

		if (#objects.blood > 0 and death == 1) then
			for i = 1, #objects.blood do

				for j = 1, 50 do
					love.graphics.setColor(0, 0, 0)
					love.graphics.circle("fill", objects.blood[i].x[j]- camera_x, objects.blood[i].y[j]- camera_y, j/7, 10)
				end

				for j = 1, 49 do
					objects.blood[i].x[j] = objects.blood[i].x[j+1]
					objects.blood[i].y[j] = objects.blood[i].y[j+1]
				end

				objects.blood[i].x[50] = objects.blood[i].x[50] + objects.blood[i].hs
				objects.blood[i].hs = objects.blood[i].hs * 0.7
				objects.blood[i].y[50] = objects.blood[i].y[50] + objects.blood[i].ws
				objects.blood[i].ws = objects.blood[i].ws * 0.7
				objects.blood[i].ws = objects.blood[i].ws + 0.3 + objects.blood[i].k/10

			end
		end

		-- рисуем игрока
		if death == 0 then
			if (hero_state == "Splash") then
				spr_hero_splash:draw(objects.player.body:getX() - camera_x - 22, objects.player.body:getY() - camera_y - 22)
			end
			if (hero_state == "NoSplash") then
				spr_hero_nosplash:draw(objects.player.body:getX() - camera_x - 22, objects.player.body:getY() - camera_y - 22)
			end
			if (hero_state == "StayLeft") then
				spr_hero_stay_l:draw(objects.player.body:getX() - camera_x - 22, objects.player.body:getY() - camera_y - 22)
			end
			if (hero_state == "StayRight")  then
				spr_hero_stay_r:draw(objects.player.body:getX() - camera_x - 22, objects.player.body:getY() - camera_y - 22)
			end
			if (hero_state == "GoLeft") then
				spr_hero_go_l:draw(objects.player.body:getX() - camera_x - 22, objects.player.body:getY() - camera_y - 22)
			end
			if (hero_state == "GoRight")  then
				spr_hero_go_r:draw(objects.player.body:getX() - camera_x - 22, objects.player.body:getY() - camera_y - 22)
			end

			if death_alpha > 0 then
				death_alpha = death_alpha - 15
			end

		else
			if death_alpha < 320 then
				death_alpha = death_alpha + 15
			else
				if start == 2 then
					level_load()
				else
					level_load2()
				end

				death = 0
			end
		end

		love.graphics.setColor(0, 0, 0)
		love.graphics.rectangle( "fill", 0, 0, death_alpha, 480)
		love.graphics.rectangle( "fill", 640 - death_alpha, 0, death_alpha, 480)
		love.graphics.setColor(255, 255, 255)
		task:draw(100, 440)	--задание
	end
end